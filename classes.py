from typing import List, Union

from game.classes import Board


class Room:
    """
    class for room - all actions with game room
    """

    def __init__(self, creator_id: int, password: Union[str, None]):
        self._creator_id = creator_id
        self._password = password
        self._status = "waiting"
        self._players = [
            creator_id,
        ]
        self._board = None
        self._current_move = None

    def get_current_move(self) -> Union[None, str]:
        """get current move in game"""
        return self._current_move

    def set_current_move(self, color: str) -> None:
        """set current move in game"""
        self._current_move = color

    def get_board(self) -> Board:
        """get in-game board"""
        return self._board

    def set_board(self, board: Board) -> None:
        """set board"""
        self._board = board

    def get_password(self) -> Union[str, None]:
        """get password"""
        return self._password

    def get_status(self) -> str:
        """get current status"""
        return self._status

    def set_status(self, new_status: str) -> None:
        """set new status"""
        self._status = new_status

    def add_player(self, new_player_id: int) -> None:
        """add player in game"""
        self._players.append(new_player_id)

    def delete_player(self, player_to_del: int) -> None:
        """delete player in game"""
        self._players.remove(player_to_del)

    def get_creator_id(self) -> int:
        """get id of game room creator"""
        return self._creator_id

    def get_players(self) -> List[int]:
        """get players in game"""
        return self._players


class Location:
    """
    class for location
    """

    def __init__(self, title: str, location_id: int = None):
        self._title = title
        self._location_id = location_id

    def get_title(self) -> str:
        """get title"""
        return self._title

    def get_location_id(self) -> int:
        """get id of location"""
        return self._location_id

    def get_title_and_location_id(self) -> tuple:
        """get both title and location id"""
        return self._title, self._location_id

    def set_title_and_location_id(self, new_title: str, new_loc_id: int = None) -> None:
        """set location id and title"""
        self._title, self._location_id = new_title, new_loc_id


class Client:
    """
    class for Client
    contains functions for general info about user:
    """

    def __init__(
            self,
            location: Location = Location(title="server"),
            victories_count: int = 0,
            losses_count: int = 0,
    ):
        self._location = location
        self._victories_count = victories_count
        self._losses_count = losses_count
        self._friends = []
        self._figure_color = None

    def victories_count_increase(self, num=1) -> None:
        """increase count of victories if player won game"""
        self._victories_count += num

    def get_figure_color(self):
        """get color of figure"""
        return self._figure_color

    def set_figure_color(self, color: str):
        """set figure color"""
        self._figure_color = color

    def add_friend(self, new_friend_id):
        """add friend by id"""
        self._friends.append(new_friend_id)

    def get_all_info(self) -> tuple:
        """all info about player"""
        return (
            ("Location", self._location.get_title()),
            ("Victories count", self._victories_count),
            ("Losses count", self._losses_count),
            ("Friends", self._friends),
        )

    def set_location(self, new_location: Location) -> None:
        """set location"""
        self._location = new_location

    def get_location(self):
        """get location"""
        return self._location
