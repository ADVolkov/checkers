from database import clients_database, rooms_database
from dictionaries import (
    commands_dict,
    help_messages,
    notifications,
)


def register_command(*locations):
    def wrapper(func):
        """
        wrapper for all input commands, which provides correct usage of functions
        ( you cant use some of them in incorrect situation)
        """
        for loc in locations:
            commands_dict[loc][func.__name__] = func
        return func

    return wrapper


@register_command("game")
def surrender(client_id: int, *args) -> str:
    """
    function for surrender
    """
    if args:
        pass

    winner = client_id
    for player_id in rooms_database.get_room_by_player_id(client_id).get_players():
        if player_id != client_id:
            winner = player_id
            clients_database.get_player_by_id(player_id).victories_count_increase()
    rooms_database.endgame(client_id)
    result = f"Player # {client_id} gave up. Player # {winner} wins!\n\n"
    notifications[winner] = result
    return result


@register_command("game")
def jump(client_id: int, move_from: str, *move_to: str) -> str:
    """
    jump function - player's movement on board
    """
    return rooms_database.make_move(client_id, move_from, move_to, is_move=False)


@register_command("game")
def move(client_id: int, move_from: str, move_to: str, *args) -> str:
    """
         move function - player's movement on board
    """
    if args:
        pass
    return rooms_database.make_move(client_id, move_from, move_to, is_move=True)


@register_command("room")
def start_game(client_id: int, *args):
    """
    function to start game by room's owner
    """
    if rooms_database.get_room_status_by_client(client_id) != "preparation":
        return "There are not enough players in the room.\n\n"
    if rooms_database.get_room_creator_by_player(client_id) != client_id:
        return "You do not have sufficient rights for this command.\n\n"
    return rooms_database.start_game(client_id)


@register_command("room")
def kick(client_addr: int, kicked_player, *_) -> str:
    """
    function for kick player from room
    """
    if int(kicked_player) not in rooms_database.get_room_players_by_player_id(
            client_addr
    ):
        return f"User with ID {kicked_player} is not in this room\n\n"
    elif int(kicked_player) == rooms_database.get_room_creator_by_player(client_addr):
        return "You don't have enough rights\n\n"
    rooms_database.kick_player_from_his_room(int(kicked_player))
    return "Player kicked successfully\n\n"


@register_command("room")
def invite(client_addr: int, invited_player_id, *_) -> str:
    """
    function for invite player to the room
    """
    if len(rooms_database.get_room_players_by_player_id(client_addr)) >= 2:
        return "The room is already full, you can't invite a player\n\n"
    try:
        _ = clients_database.get_player_by_id(int(invited_player_id))
    except:
        return "User with this ID does not exist\n\n"
    notifications[int(invited_player_id)] = (
        f"User # {client_addr} invites you to the room to agree, enter:\n"
        f"- connect {clients_database.get_player_location_id(client_addr)}\n\n"
    )
    return "User invited successfully\n\n"


@register_command("room")
def leave_room(client_addr: int, *_) -> str:
    """
    function for leave room
    """
    if rooms_database.get_room_creator_by_player(player=client_addr) == client_addr:
        rooms_database.remove_player_from_his_room_and_delete_this_room(
            player_id=client_addr
        )
    else:
        rooms_database.remove_player_from_his_room(player=client_addr)
    return f"You left the room.\n\n"


@register_command("general")
def helpme(client_addr: int, *_) -> str:
    """
    function for command 'myinfo' - all info about user (id, etc.)
    """
    return f"Available commands:\n{help_messages[clients_database.get_client_location_name(client_addr)]}\n\n"


@register_command("general")
def my_info(client_addr: int, *_) -> str:
    """
    function for command 'myinfo' - all info about user (id, etc.)
    """
    return clients_database.get_info_about_player(client_addr) + "\n\n"


@register_command("server")
def connect(client_addr: int, room_id, password=None, *_) -> str:
    """
    function for connect to room
    """
    try:
        room = rooms_database.get_room_by_id(int(room_id))
    except:
        return "Such a room does not exist\n\n"
    if room.get_status() != "waiting":
        return "This room is not available\n\n"
    if room.get_password() is not None and room.get_password() != password:
        return "The password is incorrect\n\n"

    rooms_database.connect_player_to_room(client_addr, int(room_id))
    return f"You have successfully connected to the room\n\n"


@register_command("server")
def games_list(client_addr: int, *_) -> str:
    """
    function with command for list of all available games
    """

    try:
        rooms = rooms_database.get_all_free_rooms()
    except Exception:
        return "No connection rooms available\n\n"
    return f"Rooms waiting for the player:\n{rooms}\n\n"


@register_command("server")
def create_room(client_addr: int, room_type="public", password=None) -> str:
    """
    function for create room by user
    """
    if room_type == "private" and password is None:
        raise ValueError("You must enter a password for the private room.\n\n")
    if room_type == "public":
        password = None

    rooms_database.create_new_room(creator_id=client_addr, password=password)
    return f"Room # {clients_database.get_player_location_id(player=client_addr)} was created\n\n"
