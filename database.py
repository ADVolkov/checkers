import abc
from typing import List, Union

from classes import Location, Client, Room
from dictionaries import notifications
from game.classes import Board
from game.main import (
    initialize,
    random_color_distribution,
    is_game_finished,
    get_hints,
    apply_move,
    move_error,
    hasjump_error,
    get_winner,
    apply_capture,
    jump_error,
)


class DatabaseInterface(abc.ABC):
    pass


class DatabaseClientData(DatabaseInterface):
    """
    db interface which gets all client data (position, info about player, etc.)
    """

    def __init__(self):
        """init"""
        self._db = {}

    def get_client_location_name(self, client_id: int) -> str:
        """gets current location """
        return self._db[client_id].get_location().get_title()

    def get_player_location_id(self, player: Union[int, Client]) -> int:
        """gets player's location"""
        if isinstance(player, int):
            return self._db[player].get_location().get_location_id()
        else:
            return player.get_location().get_location_id()

    def set_player_location_by_player(
            self, player: Union[int, Client], new_location: tuple
    ) -> None:
        """player can set location"""
        if isinstance(player, int):
            self._db[player].set_location(
                Location(title=new_location[0], location_id=new_location[1])
            )
        else:
            player.set_location(
                Location(title=new_location[0], location_id=new_location[1])
            )

    def get_info_about_player(self, player: int) -> str:
        """get all info about player """
        return f"ID - {player}\n" + "\n".join(
            [f"{name} - {value}" for name, value in self._db[player].get_all_info()]
        )

    def set_new_client(self, client_id):
        """setting new client"""
        self._db[client_id] = Client()

    def get_player_by_id(self, player_id) -> Client:
        """get player by id"""
        return self._db[player_id]


class DatabaseRoom(DatabaseInterface):
    """
    another db interface containing data about game rooms and all actions with game room
    """

    def __init__(self):
        self._db = {}

    def get_room_creator_by_player(self, player: int) -> int:
        """get room creator"""
        return self._db[
            clients_database.get_player_by_id(player).get_location().get_location_id()
        ].get_creator_id()

    def get_room_players_by_player_id(self, client_addr: int) -> List[Client]:
        """get info about all players in the room"""
        return self._db[
            clients_database.get_player_by_id(client_addr)
                .get_location()
                .get_location_id()
        ].get_players()

    def get_room_by_id(self, room_id: int) -> Room:
        """get room by id of the room"""
        return self._db[room_id]

    def get_all_rooms(self) -> dict:
        """get all rooms"""
        return self._db

    def delete_player_from_room(self, client_id: int) -> None:
        """delete chosen player from room"""
        self._db[
            clients_database.get_player_by_id(client_id)
                .get_location()
                .get_location_id()
        ].delete_player(client_id)

    def set_room_status_by_client(self, client_id: int, new_status: str) -> None:
        """set room status"""
        self._db[clients_database.get_player_location_id(client_id)].set_status(
            new_status
        )

    def get_room_status_by_client(self, client_id: int):
        """get room status by client"""
        return self._db[clients_database.get_player_location_id(client_id)].get_status()

    def delete_room_by_id(self, room_id: int) -> None:
        """delete room"""
        del self._db[room_id]

    def connect_player_to_room(self, player: int, room_id: int) -> None:
        """connect player to the selected room"""
        self._db[int(room_id)].add_player(player)
        self._db[int(room_id)].set_status("preparation")
        clients_database.set_player_location_by_player(player, ("room", int(room_id)))
        notifications[
            self._db[int(room_id)].get_creator_id()
        ] = f"Player # {player} connected into your room\n\n"

    def get_all_free_rooms(self) -> str:
        """returns list of free rooms"""
        return "\n".join(
            [
                f"Room # {key} - User # {value.get_creator_id()}\n"
                for key, value in self.get_all_rooms().items()
                if value.get_status() == "waiting"
            ]
        )

    def create_new_room(self, creator_id: int, password: str) -> None:
        """create new room and increase total amount of rooms"""
        rooms_count = len(self._db)
        self._db[rooms_count + 1] = Room(creator_id, password)
        clients_database.set_player_location_by_player(
            creator_id, ("room", rooms_count + 1)
        )

    def kick_player_from_his_room(self, player: int) -> None:
        """kick player from room"""
        self.set_room_status_by_client(player, "preparation")
        self.delete_player_from_room(player)
        clients_database.set_player_location_by_player(player, ("server", None))
        notifications[player] = "You are kicked out of the room\n\n"

    def remove_player_from_his_room(self, player: int) -> None:
        """remove player from room"""
        self.set_room_status_by_client(player, "waiting")
        self.delete_player_from_room(player)
        clients_database.set_player_location_by_player(player, ("server", None))

    def remove_player_from_his_room_and_delete_this_room(self, player_id: int) -> None:
        """if creator of room leave this room, room will be deleted"""
        loc_to_del = clients_database.get_player_location_id(player_id)
        for player in self.get_room_players_by_player_id(player_id):
            clients_database.set_player_location_by_player(player, ("server", None))
        self.delete_room_by_id(loc_to_del)

    def get_room_by_player_id(self, player_id: int) -> Room:
        """get info about room by player id"""
        return self.get_room_by_id(
            clients_database.get_player_by_id(player_id)
                .get_location()
                .get_location_id()
        )

    def start_game(self, creator_id: int) -> str:
        """start game"""
        self.set_room_status_by_client(creator_id, new_status="gaming")
        for player in self.get_room_players_by_player_id(creator_id):
            clients_database.set_player_location_by_player(
                player,
                new_location=("game", clients_database.get_player_location_id(player)),
            )
        creator_color, opponent_color = random_color_distribution()
        current_move = creator_color if creator_color == "white" else opponent_color
        clients_database.get_player_by_id(creator_id).set_figure_color(creator_color)
        clients_database.get_player_by_id(
            self.get_room_players_by_player_id(creator_id)[1]
        ).set_figure_color(opponent_color)

        board = Board()
        initialize(board)
        room = self.get_room_by_player_id(creator_id)
        room.set_board(board)
        room.set_current_move(current_move)

        first_move_player_id = creator_id
        for player in rooms_database.get_room_players_by_player_id(creator_id):
            if player != creator_id:
                if (
                        clients_database.get_player_by_id(player).get_figure_color()
                        == "white"
                ):
                    first_move_player_id = player
                notifications[player] = (
                    f"User # {first_move_player_id} plays first.\n{board.display()}\n"
                )

        return f"User # {first_move_player_id} plays first.\n{board.display()}\n"

    @staticmethod
    def display_result(room: Room, result: str) -> str:
        """see the result of game"""
        for player_id in room.get_players():
            if player_id != player_id:
                notifications[player_id] = result
        return result

    def endgame(self, player_id: int) -> None:
        """end game and move player to the lobby"""
        self.set_room_status_by_client(player_id, new_status="preparation")
        for player in self.get_room_players_by_player_id(player_id):
            clients_database.set_player_location_by_player(
                player,
                new_location=("room", clients_database.get_player_location_id(player)),
            )

    def make_move(
            self, player_id: int, move_from: str, move_to: str, is_move: bool
    ) -> str:
        """make move in game"""
        room = self.get_room_by_player_id(player_id)
        current_move = room.get_current_move()
        board = room.get_board()
        if (
                room.get_current_move()
                != clients_database.get_player_by_id(player_id).get_figure_color()
        ):
            raise ValueError("Your opponent makes the move now.\n\n")
        if not is_game_finished(board):
            try:
                moves, captures = get_hints(board, current_move, is_sorted=True)

                if is_move:
                    if not captures:
                        action = move_from, move_to
                        if action in moves:
                            apply_move(board, action)
                        else:
                            raise RuntimeError(move_error)
                    else:
                        raise RuntimeError(hasjump_error)
                else:
                    action = [move_from, *move_to]
                    if action in captures:
                        apply_capture(board, action)
                    else:
                        raise RuntimeError(jump_error)

                if current_move == "black":
                    room.set_current_move("white")
                else:
                    room.set_current_move("black")
                for player in self.get_room_players_by_player_id(player_id):
                    if player != player_id:
                        notifications[player] = f"{board.display()}{room.get_current_move()} turn:\n\n"
            except Exception:
                raise ValueError("Сoordinates are incorrect\n\n")

            return f"{board.display()}{room.get_current_move()} turn:\n\n"

        self.endgame(player_id)
        win_color = get_winner(board)
        winner = player_id
        for player_id in room.get_players():
            if (
                    clients_database.get_player_by_id(player_id).get_figure_color()
                    == win_color
            ):
                winner = player_id
                clients_database.get_player_by_id(player_id).victories_count_increase()
        if win_color != "draw":
            result = f"{board.display()}Player # {winner} wins!\n\n"
            return self.display_result(room, result)
        else:
            result = f"{board.display()}This game ends in a draw.\n\n"
            return self.display_result(room, result)


clients_database = DatabaseClientData()
rooms_database = DatabaseRoom()
