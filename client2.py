import socket

import settings

HOST = settings.HOST
PORT = settings.PORT

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    read_file = s.makefile(mode="r", encoding="utf-8")
    write_file = s.makefile(mode="w", encoding="utf-8")

    while True:
        data = read_file.readline().strip()

        while data:
            print(data)
            data = read_file.readline().strip()
            if data == "":
                break

        a = input()

        if a == "exit1":
            break

        write_file.write(a + "\n")
        write_file.flush()

    s.close()
