from typing import List, Union


class Figure:
    """This class contains methods and attributes for the figure object - pawns or king"""

    symbols = ["b", "w"]
    symbols_king = ["B", "W"]

    def __init__(self, color="black", is_king=False):
        if color in ("black", "white"):
            self._color = color
            self._is_king = is_king
        else:
            raise ValueError("Figure must be 'white' or 'black'.")

    def get_color(self) -> str:
        """Return color of the figure"""
        return self._color

    def is_black(self) -> bool:
        """Check is the figure black-colored Boolean function"""
        return self._color == "black"

    def is_white(self) -> bool:
        """Check is the figure white-colored Boolean function"""
        return self._color == "white"

    def is_king(self) -> bool:
        """Check is the figure king"""
        return self._is_king

    def make_king(self) -> None:
        """Change the pawn into king"""
        self._is_king = True

    def __str__(self):
        """Return string representation of the figure"""
        if self._is_king:
            return (
                self.symbols_king[0] if self._color == "black" else self.symbols_king[1]
            )
        else:
            return self.symbols[0] if self._color == "black" else self.symbols[1]

    def __repr__(self):
        return self.__str__()


class Board:
    """This class contains methods and attributes for the board object"""

    def __init__(self, size: int = 8):
        self._size = size
        self._cells = [[None for _ in range(self._size)] for _ in range(self._size)]

    def get_size(self) -> int:
        """Return size of the board"""
        return self._size

    def get_cells(self) -> List[List[Union[Figure, None]]]:
        """Return list of cells in the board"""
        return self._cells

    def cell_is_free(self, row: int, col: int) -> bool:
        """Check position in the board(free or not) - boolean function"""
        return self._cells[row][col] is None

    def set_cell(self, row: int, col: int, model: Union[Figure, None]) -> None:
        """Set value in cell"""
        self._cells[row][col] = model

    def get_cell(self, row: int, col: int) -> Union[Figure, None]:
        """Return values(list) of the cells"""
        return self._cells[row][col]

    def delete_cell(self, row: int, col: int) -> None:
        """Clear the cell in the board"""
        self._cells[row][col] = None

    def display(self) -> str:
        """Display the board string representation in terminal"""
        return f"{self}"

    def __str__(self):
        """Return string representation of rhe board"""
        horizontal_line = "\n---+" + "-----+" * self._size +  "\n"
        num_line = "------" + "-----".join([f"{str(i)}" for i in range(1, self._size + 1)]) + "---"
        result = num_line + horizontal_line
        for i in range(self._size):
            result += (
                chr(97 + i)
                + "  |"
                + "".join(
                    [
                        f"  {(str(self._cells[i][j]) if self._cells[i][j] is not None else ' ')}  |"
                        for j in range(self._size)
                    ]
                )
                + horizontal_line
            )
        return result

    def __repr__(self):
        return self.__str__()
