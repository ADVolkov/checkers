# Покрытие тестами - 86 %

# Классические шашки онлайн
*далее считаем что все запросы оканчиваются символом \n*
## Описание

- Работает на протоколе TCP
- Поддержка нескольких комнат одновременно
- Есть возможность создать комнату или присоединиться к существующей
- Рейтинг между игроками ( планируется )
- Возможность создавать приватную комнату с паролем (the password is ...)
- Один пользователь может создать 1 комнату


## Команды
## *Основные:*

`helpme`  
**показывает все доступные в данной области команды ( например в игре )**

`my_info`  
**инфа о пользователе**
 
 >**ID - *какой то id*  
Location -  *локация в памяти*  
Victories count - 0  
Losses count - 0   
Friends - []  -*список друзей***


## *На сервере:*

`create_room <private or public> <password if private>`  
**создание комнаты**

>**Room # 1 was created**


`games_list`  
**список комнат**

>**Rooms waiting for the player:**
>
>*далее список доступных комнат*
>
>**Room # 1 - User # 63510**

`connect <id>`  
**присоединиться к комнате по ее id**

>**You have successfully connected to the room**
>или
>**Such a room does not exist**


## *В игровой комнате*

`start_game`  

**старт игры**

>**User # 57809 plays first.**

***далее идет пример поля с игрой:***

![table](checker_table.png)

`invite <player id>`  
**пригласить игрока по id**

>**User invited successfully**
или
**User with this ID does not exist**

`kick <player_id>`  
**кик игрока из комнаты**

>**Player kicked successfully**
или
**User with ID (какой то id) is not in this room**

`leave_room`
**покинуть комнату**

>**You left the room.**


## *В игре*

`jump <move_from> <move to> `  
`move <move_from> <move to> ` 

**команды для движения шашками**  

*(jump- "скушать" соперника, move - движение по полю)*

- *если прописать команду не тому игроку*
>**Your opponent makes the move now.**

- *если попытаться написать неправильные координаты*
>**Сoordinates are incorrect**

`surrender`  
**сдаться**

>**Player # 54910 gave up. Player # 54911 wins!**




## Порт сервера
```sh
127.0.0.1:6666
```


## License

MIT by Timour Khalimdarov
