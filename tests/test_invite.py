from commands import invite, create_room
from database import clients_database


def test_invite():
    clients_database.set_new_client(1)
    clients_database.set_new_client(2)
    create_room(client_addr=2)
    assert invite(client_addr=2, invited_player_id=1)
