from commands import my_info
from database import clients_database


def test_my_info():
    clients_database.set_new_client(1)
    assert (
        my_info(1)
        == "ID - 1\nLocation - server\nVictories count - 0\nLosses count - 0\nFriends - []\n\n"
    )
