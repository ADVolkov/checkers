from commands import surrender, create_room, connect, start_game
from database import clients_database


def test_surrender():
    clients_database.set_new_client(5)
    clients_database.set_new_client(6)
    create_room(client_addr=6)
    connect(client_addr=5, room_id=7)
    start_game(client_id=6)
    assert surrender(6) == "Player # 6 gave up. Player # 5 wins!\n\n"
