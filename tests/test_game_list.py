import pytest

from commands import games_list
from database import rooms_database, clients_database, DatabaseClientData, DatabaseRoom


def test_game_list_type_errors():
    with pytest.raises(TypeError):
        assert (
            games_list()
            == "games_list() missing 1 required positional argument: 'client_addr'"
        )


def test_games_list_correct():
    clients_database.set_new_client(1)
    rooms_database.create_new_room(creator_id=1, password=None)
    assert (
        games_list(client_addr=1)
        == "Rooms waiting for the player:\nRoom # 2 - User # 1\n\nRoom # 3 - User # 2\n\nRoom # 4 - User # 100\n\nRoom # 5 - User # 1\n\n\n"
    )
