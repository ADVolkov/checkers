import pytest

from commands import create_room
from database import clients_database


def test_create_room_value_errors():
    with pytest.raises(ValueError):
        assert (
            create_room(client_addr=25, room_type="private")
            == "You must enter a password for the private room.\n\n"
        )


def test_create_room_correct():
    clients_database.set_new_client(1)
    clients_database.set_new_client(2)
    clients_database.set_new_client(100)
    assert create_room(1, "public", None) == "Room # 2 was created\n\n"
    assert create_room(2, "private", 123) == "Room # 3 was created\n\n"
    assert create_room(100, "public", 100) == "Room # 4 was created\n\n"
