from commands import helpme
from database import clients_database


def test_helpme():
    clients_database.set_new_client(1)
    assert (
        helpme(1)
        == "Available commands:\ncreate_room <private or public> <password if private> - create new room\ngames_list - get all available rooms\nconnect <id> <password if room is private> - connect to the game\n\n"
    )
