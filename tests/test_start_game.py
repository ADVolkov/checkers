from commands import start_game, create_room, connect
from database import clients_database, rooms_database


def test_start_game():
    clients_database.set_new_client(5)
    clients_database.set_new_client(6)
    create_room(client_addr=6)
    connect(client_addr=5, room_id=7)
    assert (
            start_game(client_id=6)
            == "User # 5 plays first.\n------1-----2-----3-----4-----5-----6-----7-----8---\n---+-----+-----+-----+-----+-----+-----+-----+-----+\na  |     |  b  |     |  b  |     |  b  |     |  b  |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nb  |  b  |     |  b  |     |  b  |     |  b  |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nc  |     |  b  |     |  b  |     |  b  |     |  b  |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nd  |     |     |     |     |     |     |     |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\ne  |     |     |     |     |     |     |     |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nf  |  w  |     |  w  |     |  w  |     |  w  |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\ng  |     |  w  |     |  w  |     |  w  |     |  w  |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nh  |  w  |     |  w  |     |  w  |     |  w  |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\n\n"
    )
