from game.classes import Board, Figure
from game.main import apply_capture, is_game_finished, get_winner

BOARD_CELLS = [
        [None, Figure(), None, Figure(), None, Figure(), None, Figure()],
        [Figure(), None, Figure(), None, Figure(), None, Figure(), None],
        [None, Figure(), None, Figure(), None, None, None, Figure()],
        [None, None, None, None, Figure(), None, None, None],
        [None, None, None, Figure("white"), None, None, None, None],
        [Figure("white"), None, None, None, Figure("white"), None, Figure("white"), None],
        [None, Figure("white"), None, Figure("white"), None, Figure("white"), None, Figure("white")],
        [Figure("white"), None, Figure("white"), None, Figure("white"), None, Figure("white"), None],
    ]


def test_apply_capture():
    board = Board()
    board._cells = BOARD_CELLS
    apply_capture(board=board, capture_path=["e4", "c6"])
    assert board.display() == "------1-----2-----3-----4-----5-----6-----7-----8---\n---+-----+-----+-----+-----+-----+-----+-----+-----+\na  |     |  b  |     |  b  |     |  b  |     |  b  |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nb  |  b  |     |  b  |     |  b  |     |  b  |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nc  |     |  b  |     |  b  |     |  w  |     |  b  |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nd  |     |     |     |     |     |     |     |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\ne  |     |     |     |     |     |     |     |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nf  |  w  |     |     |     |  w  |     |  w  |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\ng  |     |  w  |     |  w  |     |  w  |     |  w  |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\nh  |  w  |     |  w  |     |  w  |     |  w  |     |\n---+-----+-----+-----+-----+-----+-----+-----+-----+\n"


def test_is_game_finished():
    board = Board()
    board._cells = BOARD_CELLS
    assert not is_game_finished(board=board)


def test_get_winner():
    board = Board()
    board._cells = BOARD_CELLS
    assert get_winner(board) == "white"
