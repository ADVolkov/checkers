from commands import create_room, connect
from database import clients_database


def test_connect_correct():
    clients_database.set_new_client(1)
    clients_database.set_new_client(2)
    create_room(client_addr=1, room_type="public")
    assert (
        connect(client_addr=2, room_id=1)
        == f"You have successfully connected to the room\n\n"
    )
