from commands import leave_room, connect, create_room
from database import clients_database


def test_leave_room():
    clients_database.set_new_client(1)
    clients_database.set_new_client(2)
    create_room(client_addr=2)
    connect(client_addr=1, room_id=2)
    assert leave_room(1) == "You left the room.\n\n"
