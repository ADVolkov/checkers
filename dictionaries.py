"""
this file contains all available commands - user can see them using "helpme" command

these tips located in help_messages dictionary

for example, when player in game room he can use command "helpme" and program will write him
all available commands for room: start game, invite, kick, leave room

"""

commands_dict = {"server": {}, "room": {}, "general": {}, "game": {}}
notifications = {}
help_messages = {
    "general": "helpme - list of available commands\n"
    "my_info - info about current player",
    "server": "create_room <private or public> <password if private> - create new room\n"
    "games_list - get all available rooms\n"
    "connect <id> <password if room is private> - connect to the game",
    "room": "start_game - start a game in room\n"
    "invite <player id> - invite a player with selected id\n"
    "kick <player_id> - kick player with selected id\n"
    "leave_room - leave the room",
    "game": "surrender - surrender xD\n"
    "move  <move_from> <move to> - move figure\n"
    "jump  <move_from> <move to> - jump figure",
}
