import os
from dotenv import load_dotenv

load_dotenv()

"""
default settings for host, port, and number of connections
"""

HOST = os.environ.get("HOST", "127.0.0.1")
PORT = os.environ.get("PORT", 6666)
CONNECTIONS_NUMBER = os.environ.get("CONNECTIONS_NUMBER", 5)
